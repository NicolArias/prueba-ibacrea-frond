import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Tiendita } from '../components/Tiendita';
import { Inicio } from '../components/Inicio';



export const DashboardRoutes = () => {
    return (
        <>

            <div>
                <Switch>
                    <Route exact path="/tiendita" component={ Tiendita } />
                    <Route exact path="/inicio" component={ Inicio } />

                    <Redirect to="/login"/>

                </Switch>
            </div>

        </>
    )
}
