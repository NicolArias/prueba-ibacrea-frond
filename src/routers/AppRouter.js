import React from 'react';
import '../assets/styles/index.css';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';


import { Inicio } from '../components/Inicio';
import { Login } from '../components/Login';
import { DashboardRoutes } from '../routers/DashboardRoutes';



export const AppRouter = () => {

    return (
        <Router>
            <div>
                <Switch>
                    <Route exact path="/login" component={ Login } />
                    <Route path="/" component={ DashboardRoutes } />
                </Switch>
            </div>
        </Router>
    )
}
