import React from 'react';
import '../assets/styles/carousel.css';
import arrow2 from '../assets/img/previous2.png';
import arrow_left from '../assets/img/previous_left.png';
import arrow_right from '../assets/img/previous_right.png';
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import OwlCarousel from 'react-owl-carousel';

export const Carousel = () => {
    const options = {
        loop: true,
        margin: 30,
        responsiveClass: true,
        nav: true,
        autoplay: true,
        navText: false,
        smartSpeed: 1000,
        responsive: {
            0: {
                items: 1,
            },
            300: {
                items: 1,
            },
            400: {
                items: 1,
            }
        },
    };

    return (
        <div className="content_carousel">
            <img className="arrow-left" src={arrow_left} alt="" />
            <div className="item">
                    <p className="m_title">Tu consumo en almuerzos</p>
                    <p className="mes">mes Marzo<img src={arrow2} alt=""/> &nbsp;</p>
                    <div className="sub_item">
                        <div className="sub_itme1">
                            <div className="subtitle1"><p>Consumo</p></div>
                            <div className="subtitle2"><p>15</p></div>
                            <div className="subtitle3"><p>almuerzos</p></div>
                        </div>
                        <div className="sub_itme2">
                            <div className="subtitle1"><p>Tu deuda pendiente</p></div>
                            <div className="subtitle2"><p>$ 5.000</p></div>
                        </div>
                    </div>
            </div>
            <img className="arrow-right" src={arrow_right} alt="" />
            {/* <OwlCarousel className='owl-theme' {...options}>
                <div className="item">
                    <p className="m_title">Tu consumo en almuerzos</p>
                    <p className="mes">mes Marzo<img src={arrow2} alt=""/> &nbsp;</p>
                    <div className="sub_item">
                        <div className="sub_itme1">
                            <div className="subtitle1"><p>Consumo</p></div>
                            <div className="subtitle2"><p>15</p></div>
                            <div className="subtitle3"><p>almuerzos</p></div>
                        </div>
                        <div className="sub_itme2">
                            <div className="subtitle1"><p>Tu deuda pendiente</p></div>
                            <div className="subtitle2"><p>$ 5.000</p></div>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <p className="m_title">Tu consumo en almuerzos</p>
                    <p className="mes">mes Marzo<img src={arrow2} alt=""/> &nbsp;</p>
                    <div className="sub_item">
                        <div className="sub_itme1">
                            <div className="subtitle1"><p>Consumo</p></div>
                            <div className="subtitle2"><p>20</p></div>
                            <div className="subtitle3"><p>almuerzos</p></div>
                        </div>
                        <div className="sub_itme2">
                            <div className="subtitle1"><p>Tu deuda pendiente</p></div>
                            <div className="subtitle2"><p>$ 15.000</p></div>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <p className="m_title">Tu consumo en almuerzos</p>
                    <p className="mes">mes Marzo<img src={arrow2} alt=""/> &nbsp;</p>
                    <div className="sub_item">
                        <div className="sub_itme1">
                            <div className="subtitle1"><p>Consumo</p></div>
                            <div className="subtitle2"><p>15</p></div>
                            <div className="subtitle3"><p>almuerzos</p></div>
                        </div>
                        <div className="sub_itme2">
                            <div className="subtitle1"><p>Tu deuda pendiente</p></div>
                            <div className="subtitle2"><p>$ 15.000</p></div>
                        </div>
                    </div>
                </div>
            </OwlCarousel> */}
        </div>
    )
}
