import React from 'react';
import '../assets/styles/inicio.css';

import user from '../assets/img/user.png'


export const User = () => {
    return (
        <div className="content_user">
            <img className="img_user" src={user} alt=""/>
            <p className="p_val">$ 20.000</p>
            <p className="p_total">Deuda Total</p>
        </div>
    )
}
