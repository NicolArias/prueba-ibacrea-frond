import React from 'react';
import '../assets/styles/inicio.css';
import { Menu } from "./Menu";
import { User } from './User';
import background from '../assets/img/fondo.png';
import arrow_left from '../assets/img/previous_left.png';
import producto from '../assets/img/productos.png';
import { Servicios_store } from './Servicios_store';
import { Link } from 'react-router-dom';

export const Tiendita = () => {
    return (
        <div className="content">
            <img className="background_img" src={background}  alt=""/>
            <Menu /> 
            <p className="name">Tiendita de la honestidad</p>
            <Link to="/inicio"><img className="home" src={arrow_left} alt=""/></Link>
            <User />
            <img className="img_producto" src={producto}/>

            <Servicios_store />
        </div>
    )
}
