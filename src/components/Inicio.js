import React from 'react';
import '../assets/styles/inicio.css'
import { Menu } from "./Menu";
import { User } from './User';
import { Carousel } from './Carousel';
import { Servicios } from './Servicios';
import background from '../assets/img/fondo.png';
import home from '../assets/img/home.png';

export const Inicio = () => {
    return (
        <div className="content">
            <img className="background_img" src={background}  alt=""/>
            <Menu /> 
            <p className="name">Hola, Manuel</p>
            <img className="home" src={home} alt=""/>
            <User />
            <Carousel />
            <Servicios />
        </div>
    )
}
