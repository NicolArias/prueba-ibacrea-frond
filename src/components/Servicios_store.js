import React from 'react';
import '../assets/styles/servicios_store.css';

import carrito from '../assets/img/grocery-cart.png'
import consumo from '../assets/img/XMLID_1083_.png'
import pagos from '../assets/img/consumer.png'
import arrow from '../assets/img/previous.png'

export const Servicios_store = () => {
    return (
        <div className="content_serv-store">
            <div className="items">
                <div className="item1_1"><img src={consumo} alt="" /></div>
                <div><p>Comprar productos</p></div>
                <div className="arrow1"><img src={arrow} alt="" /></div>
            </div>
            <div className="items">
                <div className="item1_2"><img src={carrito} alt="" /></div>
                <div><p>Historial de consumo</p></div>
                <div className="arrow2"><img src={arrow} alt="" /></div>
            </div>
            <div className="items">
                <div className="item1_3"><img src={pagos} alt="" /></div>
                <div><p>Historial de pagos</p></div>
                <div className="arrow3"><img src={arrow} alt="" /></div>
            </div>
        </div>
    )
}
