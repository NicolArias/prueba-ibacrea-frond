import React from 'react';
import { Link } from 'react-router-dom';
import '../assets/styles/servicios.css'
import store from '../assets/img/icon-store.png';
import save from '../assets/img/icon-save-money.png';
import fork from '../assets/img/icon-fork.png';

export const Servicios = () => {
    return (
        <div className="content_servi">
            <div className="serv_fork">
                <img src={fork} alt=""/>
                <p>Almuerzos</p>
            </div>
            <div className="serv_store" >
                <Link to="/tiendita">
                    <img src={store} alt=""/>
                </Link>
                <p>Tiendita</p>
            </div>
            <div className="serv_save">
                <img src={save} alt=""/>
                <p>Fondo</p>
            </div>
        </div>
    )
}
