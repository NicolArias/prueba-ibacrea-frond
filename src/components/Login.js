import React from 'react';
import '../assets/styles/login.css';
import { Link } from 'react-router-dom';
import fondo from '../assets/img/fondo_login.png';
import logo_enlace from '../assets/img/Logo-Enlace-S-04blanco.png';

export const Login = () => {
    return (
        <div className="content_login">
            <img className="fondo_login" src={fondo} />
            <img className="logo" src={logo_enlace} />
            <div className="form">
                <div><button className="boton_inicio"><Link to="/inicio">Inicio de sesión</Link></button></div>
                <div><button className="boton_registro">Registrate</button></div>
                <div className="rec"><Link>¿Olvidate la contraseña?</Link></div>
            </div>  
        </div>
    )
}
